package cn.spark.study.core;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;

import java.util.Arrays;
import java.util.List;

/**
 * 并行化集合创建RDD
 * 案例：累加 1 到 10
 */
public class ParallelizeCollection {

    public static void main(String[] args) {
        // 创建SparkConf
        SparkConf conf = new SparkConf()
                .setAppName("ParallelizeCollection")
                .setMaster("local");

        // 创建JavaSparkContext
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 数据集合
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10);

        // 使用并行化的方式创建RDD，调用SparkContext及其子类的parallelize方法
        JavaRDD<Integer> numberRDD = sc.parallelize(numbers);

        // 执行reduce算子
        int sum = numberRDD.reduce(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer num1, Integer num2) throws Exception {
                return num1 + num2;
            }
        });

        System.out.println("1到10的累加和：" + sum);

        //关闭JavaSparkContext
        sc.close();
    }

}
