package cn.spark.study.core;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

/**
 * 使用HDFS文件创建RDD
 * 案例：统计文本文件字数
 */
public class HDFSFile {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("HDFSFile");

        JavaSparkContext sc = new JavaSparkContext(conf);

        // 调用JavaSparkContext及其子类的textFile()方法，读取HDFS文件，创建RDD
        JavaRDD<String> lines = sc.textFile("hdfs://spark1:9000/spark.txt");
        JavaRDD<Integer> lineLength = lines.map(new Function<String, Integer>() {
            @Override
            public Integer call(String line) throws Exception {
                return line.length();
            }
        });
        int count = lineLength.reduce(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 +v2;
            }
        });

        System.out.println("文本文件字数：" + count);

        sc.close();
    }
}
