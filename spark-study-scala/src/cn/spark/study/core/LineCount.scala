package cn.spark.study.core

import org.apache.spark.{SparkConf, SparkContext}


object LineCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("LineCount").setMaster("local")
    val sc = new SparkContext(conf)
    val lines = sc.textFile("C:\\Users\\残月\\Desktop\\hello.txt", 1)
    val pairs = lines.map { line => (line, 1)}
    val lineCounts = pairs.reduceByKey {_ + _}

    lineCounts.foreach(lineCount => println(lineCount._1 + " appeared " + lineCount._2 + " times."))
  }
}
