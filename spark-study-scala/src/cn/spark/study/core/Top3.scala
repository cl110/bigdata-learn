package cn.spark.study.core

import org.apache.spark.{SparkConf, SparkContext}

object Top3 {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Top3").setMaster("local")
    val sc = new SparkContext(conf)
    val lines = sc.textFile("E:\\BaiduNetdiskDownload\\Spark从入门到精通（Scala编程、案例实战、高级特性、Spark内核源码剖析、Hadoop高端）\\第40讲-Spark核心编程：高级编程之topn\\文档\\top.txt")
    val pairs = lines.map(line => (line.toInt, line))
    val sortedPairs = pairs.sortByKey(false)
    val sortedNumbers = sortedPairs.map(sortedPair => sortedPair._1)
    val top3Numbers = sortedNumbers.take(3)
//    top3Numbers.foreach {num => println(num)}
    for(num <- top3Numbers) {
      println(num)
    }
  }
}
