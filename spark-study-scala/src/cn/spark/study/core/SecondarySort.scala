package cn.spark.study.core

import org.apache.spark.{SparkConf, SparkContext}

object SecondarySort {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SecondarySort").setMaster("local")
    val sc = new SparkContext(conf)
    val lines = sc.textFile("K:\\Spark从入门到精通（Scala编程、案例实战、高级特性、Spark内核源码剖析、Hadoop高端）\\第39讲-Spark核心编程：高级编程之二次排序\\文档\\sort.txt")
    val pairs = lines.map(line => (
      new SecondarySortKey(line.split(" ")(0).toInt, line.split(" ")(1).toInt),
      line
    ))

    val sortedPairs = pairs.sortByKey()

    val sortedLines = sortedPairs.map(sortedPair => sortedPair._2)

    sortedLines.foreach(sortedLine => println(sortedLine))
  }
}
