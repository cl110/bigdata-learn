package cn.spark.study.core

import org.apache.spark.{SparkConf, SparkContext}

object ParallelizeCollection {
  def main(args : Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("ParallelizeCollection")
      .setMaster("local");

    val sc = new SparkContext(conf)

    // val numbers = 1 to 10
    val numbers = Array(1,2,3,4,5,6,7,8,9,10)
    val sum = sc.parallelize(numbers, 5)
                .reduce(_ + _);
    print("1到10的累加和：" + sum)
  }
}
